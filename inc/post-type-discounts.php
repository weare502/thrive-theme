<?php

$labels = array(
	'name'               => __( 'Discounts', 'thrive' ),
	'singular_name'      => __( 'Discount', 'thrive' ),
	'add_new'            => _x( 'Add New Discount', 'thrive', 'thrive' ),
	'add_new_item'       => __( 'Add New Discount', 'thrive' ),
	'edit_item'          => __( 'Edit Discount', 'thrive' ),
	'new_item'           => __( 'New Discount', 'thrive' ),
	'view_item'          => __( 'View Discount', 'thrive' ),
	'search_items'       => __( 'Search Discounts', 'thrive' ),
	'not_found'          => __( 'No Discounts found', 'thrive' ),
	'not_found_in_trash' => __( 'No Discounts found in Trash', 'thrive' ),
	'parent_item_colon'  => __( 'Parent Discount:', 'thrive' ),
	'menu_name'          => __( 'Discounts', 'thrive' ),
);

$args = array(
	'labels'              => $labels,
	'show_in_rest'		  => true, // make this accessible to the WP-API
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array( 'category' ),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-tickets',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);

register_post_type( 'discount', $args );
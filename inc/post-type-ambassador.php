<?php

$labels = array(
	'name'               => __( 'Ambassadors', 'thrive' ),
	'singular_name'      => __( 'Ambassador', 'thrive' ),
	'add_new'            => _x( 'Add New Ambassador', 'thrive', 'thrive' ),
	'add_new_item'       => __( 'Add New Ambassador', 'thrive' ),
	'edit_item'          => __( 'Edit Ambassador', 'thrive' ),
	'new_item'           => __( 'New Ambassador', 'thrive' ),
	'view_item'          => __( 'View Ambassador', 'thrive' ),
	'search_items'       => __( 'Search Ambassadors', 'thrive' ),
	'not_found'          => __( 'No Ambassadors found', 'thrive' ),
	'not_found_in_trash' => __( 'No Ambassadors found in Trash', 'thrive' ),
	'parent_item_colon'  => __( 'Parent Ambassador:', 'thrive' ),
	'menu_name'          => __( 'Ambassadors', 'thrive' ),
);

$args = array(
	'labels'              => $labels,
	'show_in_rest'		  => true, // make this accessible to the WP-API
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-businessman',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);

register_post_type( 'ambassador', $args );
<?php
// check for Timber
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

// change views directory to templates
Timber::$locations = __DIR__ . '/templates';

class ThriveSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'wp_insert_post_data', array( $this, 'mce_anchor_class_addition' ) );

		add_action( 'init', function() {
			add_editor_style('style.css' );
		} );

		parent::__construct();
	}

	function mce_anchor_class_addition( $postarr ) {
		$postarr['post_content'] = str_replace('<a ', '<a class="external system" ', $postarr['post_content'] );
		return $postarr;
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		//$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );

		// Global ACF values for the "Main Header Content" Field Group
		$context['main_header'] = get_field('large_heading');
		$context['sub_header'] = get_field('sub_heading');
		$context['btn_text'] = get_field('button_text');
		$context['btn_link'] = get_field('button_link');
		$context['sub_btn_text'] = get_field('sub_button_text');

		return $context;
	}

	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'footer', 'Footer Navigation' );
		register_nav_menu( 'top', 'Mini Menu' );

		add_image_size( 'xlarge', 2880, 2000 );
		add_image_size( 'large-square', 500, 500, true );

		acf_add_options_page(array(
			'page_title' 	=> 'Site Options',
			'menu_title'	=> 'Layout Options',
			'menu_slug' 	=> 'thrive-site-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	function enqueue_scripts() {
		// Dependencies
		wp_enqueue_style( 'thrive-css', get_stylesheet_directory_uri() . "/style.css", array(), '20180701' );
		wp_enqueue_script( 'thrive-theme', get_template_directory_uri() . "/static/js/site-min.js", array( 'jquery' ), '20182201' );
		wp_register_style( 'jquery-ui', '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
		wp_enqueue_style( 'jquery-ui' );

		// enqueing just for the Archive page
		if( is_page('story-archive') ) {
			wp_enqueue_script( 'thrive-theme-f', get_template_directory_uri() . "/static/js/filter-min.js", array( 'jquery' ), '20182202' );
		}
	}

	function admin_head_css(){
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
			.notice-error { display: none !important; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {

		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),

			// Learn More Button
			array(
				'title' => 'Learn More Button',
				'selector' => 'a',
				'classes' => 'learn-more'
			),

			// Arrow Button, center aligned text
			array(
				'title' => 'Arrow Button Text-Center',
				'selector' => 'a',
				'classes' => 'arrow-button-header'
			),

			// Arrow Button, left aligned text
			array(
				'title' => 'Arrow Button Text-Left',
				'selector' => 'a',
				'classes' => 'arrow-button'
			),

			//  Teal colored box button
			array(
				'title' => 'Teal Box Button',
				'selector' => 'a',
				'classes' => 'box-button'
			),

			// Teal colored box button (smaller/thinner)
			array(
				'title' => 'Slim Teal Box Button',
				'selector' => 'a',
				'classes' => 'slim-button'
			),
		);

		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		return $init_array;
	}

	function register_post_types() {
		// add cpts here
		require 'inc/post-type-story.php';
		require 'inc/post-type-ambassador.php';
		require 'inc/post-type-providers.php';
		require 'inc/post-type-discounts.php';
	}
}

new ThriveSite();

// Allow use of options globally (site-wide) ---     https://timber.github.io/docs/guides/acf-cookbook/#options-page
add_filter( 'timber_context', 'thrive_timber_context'  );

function thrive_timber_context( $context ) {
    $context['options'] = get_fields('option');
    return $context;
}

// add filter for Gforms to toggle label visibility
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// mini menu is the very top, right-most menu
function thrive_render_mini_menu() {
	wp_nav_menu( array(
		'theme_location' => 'top',
		'container' => false,
		// 'menu_class' => '',
		'menu_id' => 'mini-menu-top',
	) );
}

// main site navigation at the top
function thrive_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => false,
		// 'menu_class' => '',
		'menu_id' => 'primary-menu',
	) );
}

// list of footer links to the very right in the footer
function thrive_render_footer_menu() {
	wp_nav_menu( array(
		'theme_location' => 'footer',
		'container' => false,
		// 'menu_class' => '',
		'menu_id' => 'footer-menu',
	) );
}

// Remove the editor box on pages that do not utilize it (Cleaner Look)
add_action( 'admin_init', 'remove_editor_box' );
function remove_editor_box() {
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  	if( !isset( $post_id ) ) return;

	$home = get_the_title( $post_id );
	$nutritional = get_the_title( $post_id );
	$physical = get_the_title( $post_id );
	$emotional = get_the_title( $post_id );
	$financial = get_the_title( $post_id );
	$benefits = get_the_title( $post_id );

	if( $home == 'Home' ||
		$nutritional == 'Nutritional' ||
		$physical == 'Physical' ||
		$emotional == 'Emotional' ||
		$financial == 'Financial' ||
		$benefits == 'Benefits'
	) {
		remove_post_type_support('page', 'editor');
  	}
}

// https://www.advancedcustomfields.com/resources/moving-wp-elements-content-editor-within-acf-fields/
// Move the default editor box into an acf field area
add_action( 'acf/input/admin_head', 'acf_editor_integration' );
function acf_editor_integration() {
	?>

	<script type="text/javascript">
		(function($) {
			$(document).ready(function() {
				$('.acf-field-5b96cae177c1d .acf-input').append( $('#postdivrich') );
				// if you need to add editors to other pages / posts with acf - do so here
			});
		})(jQuery);
	</script>

	<style type="text/css">
		.acf-field #wp-content-editor-tools {
			background: transparent;
			padding-top: 0;
		}
	</style>

	<?php
}

add_filter( 'wpsl_no_results', 'custom_no_results' );

function custom_no_results() {
    $output = '<p>No results found!</p>';
    $output .= '<p>Help us get started! Send us gym recommendations and we\'ll see if we can make it happen!</p>';
    return $output;
}
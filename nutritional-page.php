<?php
/**
 * Template Name: Nutritional
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['stories'] = Timber::get_posts(array( 'post_type' => 'story', 'posts_per_page' => -1, 'orderby' => 'date' ));
$context['providers'] = Timber::get_posts(array( 'post_type' => 'provider', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ));
$context['discounts'] = Timber::get_posts(array( 'post_type' => 'discount', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ));

$templates = array( 'nutritional-page.twig' );

the_post();

Timber::render( $templates, $context );
<?php
/**
 * Template Name: Home
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

// Sort posts by date in chronological order.
$context['stories'] = Timber::get_posts(array( 'post_type' => 'story', 'posts_per_page' => 6, 'orderby' => 'date' ));
$context['ambassadors'] = Timber::get_posts(array( 'post_type' => 'ambassador', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' ));

// Get the categories for the post (post_type_name.category -> will return the category checked)
$context['category'] = Timber::get_term(['taxonomy' => 'category']);

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );
<?php
/**
 * Template Name: Story Archive Page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['stories'] = Timber::get_posts(array( 'post_type' => 'story', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ));
$context[ 'category' ] = Timber::get_term(['taxonomy'=>'category']);

$templates = array('archive-story.twig');

Timber::render( $templates, $context );
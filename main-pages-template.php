<?php
/**
 * Template Name: Main Pages
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['stories'] = Timber::get_posts(array( 'post_type' => 'story', 'posts_per_page' => 6, 'orderby' => 'date' ));
$context['providers'] = Timber::get_posts(array( 'post_type' => 'provider', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ));
$context['discounts'] = Timber::get_posts(array( 'post_type' => 'discount', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ));

// Get the categories for the post (post_type_name.category -> will return the category checked)
$context['category'] = Timber::get_term(['taxonomy' => 'category']);

the_post();

$templates = array( 'main-pages-template.twig' );

Timber::render( $templates, $context );
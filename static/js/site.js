/* global jQuery */
(function($) {
    $(document).ready(function() {
		// Mobile Menu Function
		
		$(function thriveMenu() {
			$('#menu-toggle').click(function() {
				$('#primary-menu').toggleClass('menu-open');
				$(this).toggleClass('x-switch')
			});
		});

		$(function fadeVideo() {
			var playVideo = $('#play-video');
			var headerText = $('#header-text');
			var headerVideo = $('#header-video');
			var videoOverlay = $('#video-overlay');
			var close = $('#close-button');

			playVideo.click(function() {
				if( headerText.hasClass('fade-in') ) {
					headerText.removeClass('fade-in').addClass('fade-out');
					headerVideo.addClass('fade-out');
					videoOverlay.removeClass('fade-out').addClass('fade-in');
				}
			});

			close.click(function() {
				headerText.removeClass('fade-out').addClass('fade-in');
				headerVideo.removeClass('fade-out');
				videoOverlay.removeClass('fade-in').addClass('fade-out');
				//$('.overlay').trigger('pause');
				$('.overlay')[0].pause();
			});
		});

		// "Show More" Button Function
		$(function showMore() {
			$('#load-more-posts').click(function() {

				// Toggle the box class to show it
				$('.show-more-boxes').toggleClass('stagger-boxes');

				// toggle the button's text
				var btn = document.getElementById('load-more-posts');

				if( btn.innerHTML === "See More" ) {
					btn.innerHTML = "See Less";
				} else {
					btn.innerHTML = "See More";
				}
			});
		});

		// Check the URL for [?appp=3] when the site loads and change the url for the app if it's the source
		$(function changeUrl() {

			// check to see if the URL contains appp=3 (this should be true if the app is loaded since the url will have appp=3 in it)
			if ( window.location.href.indexOf('appp=3') > 0 ) {
				$anchors = $('a');

				$anchors.each(function($anchor) {
					var $this = $(this);
					var href = $this.attr('href');

					if ( href.indexOf('spsthrive.com') > 0 ) {
					// only target anchors on our current domain
					
						if ( href.indexOf('?') < 0 ) {
							// ? not found
							// append our query param
							$this.attr('href', href + "?appp=3");
						} else {
							// ? was found, so add a new parameter
							$this.attr('href', href + "&appp=3");
						}
					}  
				});
			}
		});
	}); // closing $(document.ready());
})(jQuery)
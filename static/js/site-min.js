/* global jQuery */
!function(d){d(document).ready(function(){
// Mobile Menu Function
d(function e(){d("#menu-toggle").click(function(){d("#primary-menu").toggleClass("menu-open"),d(this).toggleClass("x-switch")})}),d(function e(){var a=d("#play-video"),o=d("#header-text"),n=d("#header-video"),s=d("#video-overlay"),t=d("#close-button");a.click(function(){o.hasClass("fade-in")&&(o.removeClass("fade-in").addClass("fade-out"),n.addClass("fade-out"),s.removeClass("fade-out").addClass("fade-in"))}),t.click(function(){o.removeClass("fade-out").addClass("fade-in"),n.removeClass("fade-out"),s.removeClass("fade-in").addClass("fade-out"),
//$('.overlay').trigger('pause');
d(".overlay")[0].pause()})}),
// "Show More" Button Function
d(function e(){d("#load-more-posts").click(function(){
// Toggle the box class to show it
d(".show-more-boxes").toggleClass("stagger-boxes");
// toggle the button's text
var e=document.getElementById("load-more-posts");"See More"===e.innerHTML?e.innerHTML="See Less":e.innerHTML="See More"})}),
// Check the URL for [?appp=3] when the site loads and change the url for the app if it's the source
d(function e(){
// check to see if the URL contains appp=3 (this should be true if the app is loaded since the url will have appp=3 in it)
0<window.location.href.indexOf("appp=3")&&($anchors=d("a"),$anchors.each(function(e){var a=d(this),o=a.attr("href");0<o.indexOf("spsthrive.com")&&(
// only target anchors on our current domain
o.indexOf("?")<0?
// ? not found
// append our query param
a.attr("href",o+"?appp=3"):
// ? was found, so add a new parameter
a.attr("href",o+"&appp=3"))}))})});// closing $(document.ready());
}(jQuery);
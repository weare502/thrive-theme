<?php
/**
 * Template Name: Benefits
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['providers'] = Timber::get_posts(array( 'post_type' => 'provider', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ));

$templates = array( 'benefits.twig' );

Timber::render( $templates, $context );